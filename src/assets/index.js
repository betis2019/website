import Logo from "./logo.png";
import BackgroundHome from "./bg-home.svg";
import Intro from "./intro.png";
import Ihwan from "./ihwan.png";
import BackgroundTeaser from "./bg-teaser.svg";
import BackgroundTentang from "./bg-tentang.svg";
import Balairung from "./balairung.png";
import Fasilkom from "./fasilkom.png";
import Dummy from "./dummy.png";

export default {
  Logo,
  BackgroundHome,
  Intro,
  Ihwan,
  BackgroundTeaser,
  BackgroundTentang,
  Balairung,
  Fasilkom,
  Dummy
};
