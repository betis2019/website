import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Grid, Typography } from "@material-ui/core/";

import Assets from "../../assets";
import "./styles.scss";

class Footer extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };
  render() {
    const { classes } = this.props;

    return (
      <div className={`${classes.root} betis-footer`}>
        <Grid
          container
          spacing={0}
          justify="center"
          alignItems="center"
          className="wrapper"
        >
          <Grid item md={6} sm={12} className="footer-info">
            <Typography align="center" variant="h4">
              <i className="fab fa-line" /> @betisfasilkomui
            </Typography>
          </Grid>
          <Grid item md={6} sm={12} className="footer-sponsor">
            <img src={Assets.Dummy} alt="dummy" />
          </Grid>
        </Grid>
        <Typography align="center" variant="h6" className="copyright">
          &copy; BETIS Fasilkom UI 2019
        </Typography>
      </div>
    );
  }
}

const styles = {
  root: {
    flexGrow: 1
  }
};

export default withStyles(styles)(Footer);
