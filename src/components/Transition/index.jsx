import React, { Component } from "react";
import Fade from "@material-ui/core/Fade";

export default class Transition extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mount: false
    };
  }

  componentDidMount() {
    if (!this.state.mount) {
      this.setState({ mount: true });
    }
  }

  componentWillUnmount() {
    if (this.state.mount) {
      this.setState({ mount: false });
    }
  }

  render() {
    return <Fade in={this.state.mount}>{this.props.children}</Fade>;
  }
}
