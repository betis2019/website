import React, { Component } from "react";
import { Typography } from "@material-ui/core/";
import Assets from "../../assets";
import "./styles.scss";

export default class ComingSoon extends Component {
  render() {
    return (
      <div className="coming-soon">
        <img src={Assets.Logo} alt="Betis Logo" />
        <Typography variant="h1">Coming Soon</Typography>
      </div>
    );
  }
}
