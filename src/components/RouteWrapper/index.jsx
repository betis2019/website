import React from "react";
import PropTypes from "prop-types";
import DocumentTitle from "react-document-title";
import { withRouter } from "react-router";
import { Route, Switch } from "react-router-dom";

class Router extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,

    paths: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        route: PropTypes.shape({
          path: PropTypes.string.isRequired,
          component: PropTypes.any.isRequired,
          exact: PropTypes.boolean
        })
      })
    ),
    titleSuffix: PropTypes.string
  };

  static defaultProps = {
    titleSuffix: "BETIS Fasilkom UI 2019"
  };

  render() {
    const { paths, titleSuffix, match, children } = this.props;

    return (
      <Switch>
        {paths.map(({ title, id, route }) => (
          <Route
            key={id}
            path={`${match.path === "/" ? "" : match.url}${route.path}`}
            exact={!!route.exact}
            render={props => (
              <DocumentTitle
                title={
                  title === "Home" ? titleSuffix : title + " - " + titleSuffix
                }
              >
                <route.component {...props} />
              </DocumentTitle>
            )}
          />
        ))}
        {children}
      </Switch>
    );
  }
}

export const RouterWrapper = withRouter(Router);
export default RouterWrapper;
