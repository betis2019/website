import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import MenuIcon from "@material-ui/icons/Menu";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import { Link } from "react-router-dom";
import Assets from "../../assets";

import {
  AppBar,
  Drawer,
  Toolbar,
  Typography,
  IconButton,
  Button,
  List,
  ListItem,
  ListItemText,
  ClickAwayListener,
  Grow,
  Paper,
  Popper,
  MenuItem,
  MenuList,
  Collapse
} from "@material-ui/core/";

import "./styles.scss";

const navLink = [
  {
    title: "Beranda",
    link: "/",
    id: "beranda",
    sublink: []
  },
  {
    title: "Profil",
    link: "/profil",
    id: "profil",
    sublink: [
      {
        title: "Peserta",
        link: "/profil/peserta"
      },
      {
        title: "Mentor",
        link: "/profil/mentor"
      },
      {
        title: "Pengajar",
        link: "/profil/pengajar"
      }
    ]
  },
  {
    title: "Daftar",
    link: "/daftar",
    id: "daftar",
    sublink: [
      {
        title: "Pendaftaran Peserta",
        link: "/daftar/peserta"
      },
      {
        title: "Pendaftaran Mentor",
        link: "/daftar/mentor"
      },
      {
        title: "Pendaftaran Pengajar",
        link: "/daftar/pengajar"
      }
    ]
  },
  {
    title: "Tentang",
    link: "/tentang",
    id: "tentang",
    sublink: []
  }
];

class Navbar extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  state = {
    open: false,
    top: true,
    profil: false,
    daftar: false,
    profilList: false,
    daftarList: false
  };

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll, { passive: true });
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll, { passive: true });
  }

  handleScroll = () => {
    this.setState({
      top: window.scrollY === 0
    });
  };

  toggleDrawer = open => {
    this.setState({ open: open });

    if (!open) {
      this.setState({ daftarList: false, profilList: false });
    }
  };

  toggleMenu = id => {
    this.setState({ [id]: !this.state[id] });
  };

  toggleCloseMenu = (event, id) => {
    if (this.anchorEl[id].contains(event.target)) {
      return;
    }

    this.setState({ [id]: false });
  };

  toggleSideList = id => {
    const menuId = id + "List";
    this.setState({ [menuId]: !this.state[menuId] });
  };

  anchorEl = {};

  renderMenu = () => {
    return navLink.map((item, index) =>
      item.sublink.length === 0 ? (
        <Link to={item.link} key={index} className="navbar-link">
          <Button color="inherit">{item.title}</Button>
        </Link>
      ) : (
        <React.Fragment key={index}>
          <Button
            buttonRef={node => {
              this.anchorEl[item.id] = node;
            }}
            aria-owns={this.state[item.id] ? `menu-${item.id}` : undefined}
            aria-haspopup="true"
            onClick={e => this.toggleMenu(item.id)}
            className="navbar-link"
          >
            {item.title}
            {this.state[item.id] ? <ExpandLess /> : <ExpandMore />}
          </Button>

          <Popper
            open={this.state[item.id]}
            anchorEl={this.anchorEl[item.id]}
            transition
            disablePortal
          >
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                id={`menu-${item.id}`}
                style={{
                  transformOrigin:
                    placement === "bottom" ? "center top" : "center bottom"
                }}
              >
                <Paper>
                  <ClickAwayListener
                    onClickAway={e => this.toggleCloseMenu(e, item.id)}
                  >
                    <MenuList>
                      {item.sublink.map((subl, index) => (
                        <Link to={subl.link} key={index}>
                          <MenuItem
                            onClick={e => this.toggleCloseMenu(e, item.id)}
                          >
                            {subl.title}
                          </MenuItem>
                        </Link>
                      ))}
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Popper>
        </React.Fragment>
      )
    );
  };

  renderSideList = () => {
    const { classes } = this.props;

    return (
      <div className={classes.list}>
        <List>
          {navLink.map((item, index) =>
            item.sublink.length === 0 ? (
              <Link
                key={index}
                to={item.link}
                onClick={e => this.toggleDrawer(false)}
              >
                <ListItem button color="primary">
                  <ListItemText primary={item.title} />
                </ListItem>
              </Link>
            ) : (
              <React.Fragment key={index}>
                <ListItem
                  button
                  color="primary"
                  onClick={e => this.toggleSideList(item.id)}
                >
                  <ListItemText primary={item.title} />
                  {this.state[`${item.id}List`] ? (
                    <ExpandLess />
                  ) : (
                    <ExpandMore />
                  )}
                </ListItem>

                <Collapse
                  in={this.state[`${item.id}List`]}
                  timeout="auto"
                  unmountOnExit
                >
                  <List component="div" disablePadding>
                    {item.sublink.map((subl, index) => (
                      <Link
                        to={subl.link}
                        key={index}
                        onClick={e => this.toggleDrawer(false)}
                      >
                        <ListItem button className={classes.nested}>
                          <ListItemText
                            style={{ paddingLeft: "16px" }}
                            primary={subl.title}
                          />
                        </ListItem>
                      </Link>
                    ))}
                  </List>
                </Collapse>
              </React.Fragment>
            )
          )}
        </List>
      </div>
    );
  };

  render() {
    const { open, top } = this.state;
    const { classes } = this.props;

    return (
      <React.Fragment>
        <div className={classes.root}>
          <AppBar
            color="primary"
            className={`betis-navbar ${top ? "" : "shadow"}`}
          >
            <Toolbar>
              <IconButton
                className={`${classes.menuButton} betis-menu`}
                color="inherit"
                aria-label="Menu"
                onClick={e => this.toggleDrawer(true)}
              >
                <MenuIcon />
              </IconButton>
              <Link to="/">
                <img src={Assets.Logo} alt="Betis Logo" className="logo" />
              </Link>
              <Typography variant="h4" color="inherit" className={classes.grow}>
                <Link to="/">BETIS</Link>
              </Typography>
              {this.renderMenu()}
            </Toolbar>
          </AppBar>
        </div>

        <Drawer open={open} onClose={e => this.toggleDrawer(false)}>
          <div
            tabIndex={0}
            role="button"
            onKeyDown={e => this.toggleDrawer(false)}
            className="betis-drawer"
          >
            {this.renderSideList()}
          </div>
        </Drawer>
      </React.Fragment>
    );
  }
}

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 8
  },
  list: {
    width: 250
  },
  paper: {
    marginRight: theme.spacing.unit * 2
  }
});

export default withStyles(styles)(Navbar);
