import React, { Component } from "react";
import { Route } from "react-router-dom";
import HomePage from "./HomePage";
import PesertaPage from "./PesertaPage";
import MentorPage from "./MentorPage";
import PengajarPage from "./PengajarPage";
import PendaftaranPage from "./PendaftaranPage";
import TentangPage from "./Tentang";
import NotFoundPage from "./404NotFoundPage";
import { Navbar, RouteWrapper, Footer } from "../components";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";

class App extends Component {
  paths = [
    {
      title: "Home",
      id: "home",
      route: {
        exact: true,
        path: "/",
        component: HomePage
      }
    },
    {
      title: "Peserta",
      id: "peserta",
      route: {
        exact: true,
        path: "/profil/peserta",
        component: PesertaPage
      }
    },
    {
      title: "Mentor",
      id: "mentor",
      route: {
        exact: true,
        path: "/profil/mentor",
        component: MentorPage
      }
    },
    {
      title: "Pengajar",
      id: "pengajar",
      route: {
        exact: true,
        path: "/profil/pengajar",
        component: PengajarPage
      }
    },
    {
      title: "Pendaftaran Peserta",
      id: "pendaftaran-peserta",
      route: {
        exact: true,
        path: "/daftar/peserta",
        component: PendaftaranPage
      }
    },
    {
      title: "Pendaftaran Pengajar",
      id: "pendaftaran-pengajar",
      route: {
        exact: true,
        path: "/daftar/pengajar",
        component: PesertaPage
      }
    },
    {
      title: "Pendaftaran Mentor",
      id: "pendaftaran-mentor",
      route: {
        exact: true,
        path: "/daftar/mentor",
        component: PesertaPage
      }
    },
    {
      title: "Tentang",
      id: "tentang",
      route: {
        exact: true,
        path: "/tentang",
        component: TentangPage
      }
    }
  ];

  theme = createMuiTheme({
    palette: {
      primary: { main: "#d9ebf7" },
      secondary: { main: "#3762ad" }
    },
    typography: { useNextVariants: true }
  });

  render() {
    return (
      <div className="App">
        <MuiThemeProvider theme={this.theme}>
          <Navbar />
          <div id="container">
            <RouteWrapper paths={this.paths}>
              <Route path="*" component={NotFoundPage} />
            </RouteWrapper>
          </div>
          <Footer />
        </MuiThemeProvider>
      </div>
    );
  }
}

export default App;
