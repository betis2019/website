import React, { Component } from "react";
import { Transition } from "../../components";
import TentangSection from "./components/TentangSection";
import "./styles.scss";

export default class TentangPage extends Component {
  render() {
    return (
      <Transition>
        <div className="tentang-page">
          <TentangSection />
        </div>
      </Transition>
    );
  }
}
