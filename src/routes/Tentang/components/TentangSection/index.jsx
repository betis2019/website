import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Grid, Typography, Tabs, Tab } from "@material-ui/core/";

import Assets from "../../../../assets";
import "./styles.scss";

class LogoSection extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  state = {
    value: "one"
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    return (
      <div className={`${classes.root} tentang-section`}>
        <Tabs
          value={value}
          onChange={this.handleChange}
          indicatorColor="secondary"
          textColor="secondary"
          centered
        >
          <Tab
            value="one"
            label={<span className="tab-button">Tentang UI</span>}
          />
          <Tab
            value="two"
            label={<span className="tab-button">Tentang Fasilkom</span>}
          />
        </Tabs>
        {value === "one" && (
          <Grid
            container
            spacing={0}
            justify="center"
            alignItems="center"
            className="tentang-content"
          >
            <Grid item md={4} sm={12} className="tentang-img">
              <img src={Assets.Balairung} alt="balairung" />
            </Grid>
            <Grid item md={8} sm={12} className="tentang-desc">
              <Typography align="justify">
                Universitas Indonesia adalah kampus modern, komprehensif,
                terbuka, multi budaya, dan humanis yang mencakup disiplin ilmu
                yang luas. UI saat ini secara simultan selalu berusaha menjadi
                salah satu universitas riset atau institusi akademik terkemuka
                di dunia. Sebagai universitas riset, upaya-upaya pencapaian
                tertinggi dalam hal penemuan, pengembangan dan difusi
                pengetahuan secara regional dan global selalu dilakukan.
                Sementara itu, UI juga memperdalam komitmen dalam upayanya di
                bidang pengembangan akademik dan aktifitas penelitian melalui
                sejumlah disiplin ilmu yang ada dilingkupnya.
              </Typography>
            </Grid>
          </Grid>
        )}
        {value === "two" && (
          <Grid
            container
            spacing={0}
            justify="center"
            alignItems="center"
            className="tentang-content"
          >
            <Grid container spacing={0} justify="center" alignItems="center">
              <Grid item md={4} sm={12} className="tentang-img">
                <img src={Assets.Fasilkom} alt="balairung" />
              </Grid>
              <Grid item md={8} sm={12} className="tentang-desc">
                <Typography align="justify">
                  Faculty of Computer Science (Fasilkom UI) was established in
                  1972. It was established initially as Computing Centre
                  (Pusilkom UI) that belong to Universitas Indonesia. The
                  purpose was to develop computer science field in Indonesia,
                  which still holds true until today.
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        )}
      </div>
    );
  }
}

const styles = {
  root: {
    flexGrow: 1
  }
};

export default withStyles(styles)(LogoSection);
