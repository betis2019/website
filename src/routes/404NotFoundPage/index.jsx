import React, { Component } from "react";
import { Typography } from "@material-ui/core/";
import Assets from "../../assets";
import "./styles.scss";

export default class NotFoundPage extends Component {
  render() {
    return (
      <div className="not-found">
        <img src={Assets.Logo} alt="Betis Logo" />
        <Typography variant="h2">Halaman Tidak Ditemukan</Typography>
      </div>
    );
  }
}
