import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Grid, Typography } from "@material-ui/core/";

import "./styles.scss";

class Teaser extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={`${classes.root} betis-teaser`}>
        <Typography align="center" variant="h2">
          TEASER BETIS 2019
        </Typography>
        <Grid
          container
          spacing={0}
          justify="center"
          alignItems="center"
          className="video-container"
        >
          <iframe
            title="Teaser"
            width="1280"
            height="720"
            src="https://www.youtube.com/embed/FzRFWwl4oNI"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          />
        </Grid>
      </div>
    );
  }
}

const styles = {
  root: {
    flexGrow: 1
  }
};

export default withStyles(styles)(Teaser);
