import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Grid, Typography } from "@material-ui/core/";

import Assets from "../../../../assets";
import "./styles.scss";

class LogoSection extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={`${classes.root} logo-section`}>
        <Grid container spacing={0} justify="center" alignItems="center">
          <Grid item md={6} sm={12} className="home-bg">
            <img src={Assets.BackgroundHome} alt="" />
          </Grid>
          <Grid item md={6} sm={12} className="main-logo">
            <img src={Assets.Logo} alt="Betis Logo" />
            <Typography variant="h3">Climb Beyond The Limit</Typography>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const styles = {
  root: {
    flexGrow: 1
  }
};

export default withStyles(styles)(LogoSection);
