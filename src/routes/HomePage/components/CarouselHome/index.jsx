import React, { Component } from "react";
import Slider from "react-slick";
import { Fab, Typography, Grid } from "@material-ui/core/";
import { ChevronLeft, ChevronRight } from "@material-ui/icons/";

import "./styles.scss";

import Assets from "../../../../assets";

const data = [
  {
    image: Assets.Intro,
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec iaculis eleifend neque, imperdiet imperdiet diam aliquet aliquet. Praesent efficitur vel metus at ornare. Nam consequat arcu sapien, et maximus dolor aliquam vitae. Praesent at lectus id neque tincidunt auctor faucibus id sem. Proin hendrerit ante vitae rhoncus dictum. Pellentesque ut metus nec augue laoreet fringilla in eget odio. Vestibulum augue nulla, elementum sed magna vel, elementum fringilla turpis. Ut quis euismod libero. Nulla pulvinar felis non varius rutrum. Pellentesque dictum vel diam ut ultricies."
  },
  {
    image: Assets.Intro,
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec iaculis eleifend neque, imperdiet imperdiet diam aliquet aliquet. Praesent efficitur vel metus at ornare. Nam consequat arcu sapien, et maximus dolor aliquam vitae. Praesent at lectus id neque tincidunt auctor faucibus id sem. Proin hendrerit ante vitae rhoncus dictum. Pellentesque ut metus nec augue laoreet fringilla in eget odio. Vestibulum augue nulla, elementum sed magna vel, elementum fringilla turpis. Ut quis euismod libero. Nulla pulvinar felis non varius rutrum. Pellentesque dictum vel diam ut ultricies."
  },
  {
    image: Assets.Intro,
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec iaculis eleifend neque, imperdiet imperdiet diam aliquet aliquet. Praesent efficitur vel metus at ornare. Nam consequat arcu sapien, et maximus dolor aliquam vitae. Praesent at lectus id neque tincidunt auctor faucibus id sem. Proin hendrerit ante vitae rhoncus dictum. Pellentesque ut metus nec augue laoreet fringilla in eget odio. Vestibulum augue nulla, elementum sed magna vel, elementum fringilla turpis. Ut quis euismod libero. Nulla pulvinar felis non varius rutrum. Pellentesque dictum vel diam ut ultricies."
  }
];

function PrevArrow(props) {
  const { onClick } = props;
  return (
    <Fab
      className="arrow arrow-left"
      color="primary"
      onClick={onClick}
      size="small"
    >
      <ChevronLeft />
    </Fab>
  );
}

function NextArrow(props) {
  const { onClick } = props;
  return (
    <Fab
      className="arrow arrow-right"
      color="primary"
      onClick={onClick}
      size="small"
    >
      <ChevronRight />
    </Fab>
  );
}

export default class CarouselHome extends Component {
  render() {
    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      prevArrow: <PrevArrow />,
      nextArrow: <NextArrow />
    };

    return (
      <div className="carousel-home">
        <Slider {...settings}>
          {data.map((item, index) => (
            <div key={index}>
              <Grid
                container
                spacing={16}
                justify="center"
                alignItems="center"
                className="carousel-item"
              >
                <Grid item md={4} sm={12} className="item-img">
                  <img src={item.image} alt="" />
                </Grid>
                <Grid item md={8} sm={12} className="item-desc">
                  <Typography align="justify">{item.description}</Typography>
                </Grid>
              </Grid>
            </div>
          ))}
        </Slider>
      </div>
    );
  }
}
