import React, { Component } from "react";
import LogoSection from "./components/LogoSection/";
import CarouselHome from "./components/CarouselHome";
import Teaser from "./components/Teaser";
import { Transition } from "../../components";

export default class HomePage extends Component {
  render() {
    return (
      <Transition>
        <div className="home-page">
          <LogoSection />
          <CarouselHome />
          <Teaser />
        </div>
      </Transition>
    );
  }
}
