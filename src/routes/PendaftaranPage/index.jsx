import React, { Component } from "react";
import "./styles.scss";
import { Typography } from "@material-ui/core/";
import { Transition, Countdown } from "../../components";

export default class PendaftaranPage extends Component {
  render() {
    const date = "2018-12-28T00:00";

    return (
      <Transition>
        <div className="pendaftaran-peserta">
          <Typography variant="h2">Pendaftaran Peserta Betis 2019</Typography>
          {new Date() < new Date(date) && <Countdown date={date} />}
          {new Date() > new Date(date) && (
            <React.Fragment>
              <div className="form-pendaftaran">
                <iframe
                  title="Test Form"
                  src="https://docs.google.com/forms/d/e/1FAIpQLSdt6g1ci4hGXf6BpYJC038IoFOXIYzL7PTKKQyZmryd6bdgZg/viewform?embedded=true"
                  width="640"
                  height="772"
                  frameborder="0"
                  marginheight="0"
                  marginwidth="0"
                >
                  Loading...
                </iframe>
              </div>
            </React.Fragment>
          )}
        </div>
      </Transition>
    );
  }
}
